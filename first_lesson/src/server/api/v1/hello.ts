import {
  Request, ResponseToolkit, ResponseObject, ServerRoute, IReply,
} from '@hapi/hapi';
// Загружаем пользователей
// const users = [{ name: 'Vasya', pass: 'Vasya123', UUID: 'c42021e3-122c-43f6-a0a4-212d7b02e9d1', },
//   { name: 'Petya', pass: 'Petya1234', UUID: 'c42021e3-122c-43f6-a0a4-212d7b02e9d2', },
//   { name: 'Mark', pass: 'Mark12345', UUID: 'c42021e3-122c-43f6-a0a4-212d7b02e9d3', }];
import { users, } from './bd';

// вспомогательная функция
function helloUser(inpMass: any, inpUUID: any):string {
  for (let i = 0; i < inpMass.length; i++) {
    if (inpUUID === inpMass[i].UUID) {
      return `Hello ${inpMass[i].name}`;
    }
  }
}

export function sayHello(request: Request, h: ResponseToolkit):any {
  // return ('Hello World');
  return helloUser(users, request.params.UUID);
}
