import { sayHello, } from '../../api/v1/hello';

export default [
  {
    method: 'GET',
    path: '/v1/hello/{UUID}',
    handler: sayHello,
  }
];
