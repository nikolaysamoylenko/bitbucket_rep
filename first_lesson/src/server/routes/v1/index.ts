import user from './user';
import hello from './hello';

export default [
  ...user,
  ...hello
];
